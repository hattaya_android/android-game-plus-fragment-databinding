package buu.hattaya.plusgameproject

import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import buu.hattaya.plusgameproject.databinding.FragmentPlayBinding
import kotlin.random.Random


class PlayFragment : Fragment() {
    private lateinit var binding: FragmentPlayBinding
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate<FragmentPlayBinding>(inflater,
            R.layout.fragment_play,container,false)

        timer()
        Game()
        return binding.root

    }

    var correctScore: Int = 0
    var incorrectScore: Int = 0
    var numberOfClauses:Int = -1

    fun timer () {
        val timer = binding.txtTimer

        object : CountDownTimer(61000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                timer.setText("Time     " +millisUntilFinished / 1000)
            }
            override fun onFinish() {
                timer.setText("Time's finished !")
            }
        }.start()
    }

    fun Game() {
        numberOfClauses ++
        val number1 = binding.txtNumber1
        val numberRandom1 =  Random.nextInt(0,9)
        number1.text = numberRandom1.toString()

        val number2 = binding.txtNumber2
        val numberRandom2 =  Random.nextInt(0,9)
        number2.text = numberRandom2.toString()

        val amount = binding.txtAmount
        amount.text = numberOfClauses.toString()

        val answer: Int = numberRandom1 + numberRandom2

        val position = Random.nextInt(0,3)

        val choice1 = binding.btnChoice1
        val choice2 = binding.btnChoice2
        val choice3 = binding.btnChoice3

        if (position == 1){
            choice1.text = (answer+1).toString()
            choice2.text = (answer-1).toString()
            choice3.text = answer.toString()
        } else if (position == 2) {
            choice1.text = (answer-1).toString()
            choice2.text = answer.toString()
            choice3.text = (answer+1).toString()
        } else {
            choice1.text = answer.toString()
            choice2.text = (answer-1).toString()
            choice3.text = (answer+1).toString()
        }

        val scoreCorrect = binding.txtpointRight
        scoreCorrect.text = correctScore.toString()
        val scoreIncorrect = binding.txtpointWrong
        scoreIncorrect.text = incorrectScore.toString()

        val declare = binding.txtDeclare

        fun addCorrect(score: Int) {
            correctScore ++
            scoreCorrect.text = correctScore.toString()
            declare.text = " Correct !!"
        }
        fun addIncorrect(score: Int) {
            incorrectScore ++
            scoreIncorrect.text = incorrectScore.toString()
            declare.text = " Incorrect !!"
        }

        choice1.setOnClickListener {
            if (choice1.text.toString() == answer.toString()) {
                addCorrect(correctScore)
                Game()
            } else {
                addIncorrect(incorrectScore)
                Game()
            }
        }
        choice2.setOnClickListener {
            if (choice2.text.toString() == answer.toString()) {
                addCorrect(correctScore)
                Game()
            } else {
                addIncorrect(incorrectScore)
                Game()
            }
        }
        choice3.setOnClickListener {
            if (choice3.text.toString() == answer.toString()) {
                addCorrect(correctScore)
                Game()
            } else {
                addIncorrect(incorrectScore)
                Game()
            }
        }


    }
}